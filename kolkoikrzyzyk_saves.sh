clear
zapisano="0"
function zapisywanie_rozgrywki {
echo " echo \" Zapis ze slotu $slotdozapisu: \" " 	> zapis_$wybranyslot.sh
echo "echo \"\" " >> zapis_$wybranyslot.sh
if [ $trybb = "1" ] ; then
echo ' echo " Tryb: 1 vs komputer " ' >> zapis_$wybranyslot.sh
elif [ $trybb = "2" ] ; then
echo ' echo " Tryb: 1 vs 1 " ' >> zapis_$wybranyslot.sh
else
sleep 1
fi
if [ $poztr = "1" ] ; then
echo ' echo " Poziom trudności: łatwy " ' >> zapis_$wybranyslot.sh
elif [ $poztr = "2" ] ; then
echo ' echo " Poziom trudności: Normalny" ' >> zapis_$wybranyslot.sh
elif [ $poztr = "3" ] ; then
echo ' echo " Poziom trudności: Trudny" ' >> zapis_$wybranyslot.sh
fi
echo " echo \" Liczba wykonanych ruchów: $liczba \" " >> zapis_$wybranyslot.sh
echo 'echo "" ' >> zapis_$wybranyslot.sh
echo 'echo " Pola gry:" ' >> zapis_$wybranyslot.sh
echo 'echo ""'  >> zapis_$wybranyslot.sh
echo " echo \" $l1 | $l2 | $l3 \"" >> zapis_$wybranyslot.sh
echo " echo \"----------- \"" >> zapis_$wybranyslot.sh
echo " echo \" $l4 | $l5 | $l6\" " >> zapis_$wybranyslot.sh
echo " echo \"-----------\" " >> zapis_$wybranyslot.sh
echo " echo \" $l7 | $l8 | $l9 \"" >> zapis_$wybranyslot.sh
echo ' echo "" ' >> zapis_$wybranyslot.sh
echo " echo \" Nazwy graczy: \" " >> zapis_$wybranyslot.sh
echo ' echo "" ' >> zapis_$wybranyslot.sh
echo " echo \" Gracz 1 :  $gracz1 \" " >> zapis_$wybranyslot.sh
echo " echo \" Gracz 2 : $gracz2 \"" >> zapis_$wybranyslot.sh
echo " Rozgrywka została zapisana"
echo "declare -g l1=\"$l1\" " >> zapis_$wybranyslot.sh
echo "declare -g l2=\"$l2\" " >> zapis_$wybranyslot.sh
echo "declare -g l3=\"$l3\" " >> zapis_$wybranyslot.sh
echo "declare -g l4=\"$l4\" " >> zapis_$wybranyslot.sh
echo "declare -g l5=\"$l5\" " >> zapis_$wybranyslot.sh
echo "declare -g l6=\"$l6\" " >> zapis_$wybranyslot.sh
echo "declare -g l7=\"$l7\" " >> zapis_$wybranyslot.sh
echo "declare -g l8=\"$l8\" " >> zapis_$wybranyslot.sh
echo "declare -g l9=\"$l9\" " >> zapis_$wybranyslot.sh
echo "declare -g trybb=\"$trybb\" " >> zapis_$wybranyslot.sh
echo "declare -g poztr=\"$poztr\" " >> zapis_$wybranyslot.sh
echo "declare -g gracz1=\"$gracz1\" " >> zapis_$wybranyslot.sh
echo "declare -g gracz2=\"$gracz2\" " >> zapis_$wybranyslot.sh
echo "declare -g liczba=\"$liczba\" " >> zapis_$wybranyslot.sh
echo "declare -g gz=\"$gz\" " >> zapis_$wybranyslot.sh
echo "declare -g liczba=\"$liczba\" " >> zapis_$wybranyslot.sh

}
echo ''
echo "  Witaj w grze kółko i krzyżyk!"
echo ''
select y in "Gra" "Zobacz/Wczytaj/Usuń zapisane rozgrywki" "Zapisy" "Jak grać?" "Autor" "Wyjście"
do
 case $y in 
  "Gra")
  echo ""
  echo "Wybierz tryb gry:"
  echo ""
  wczytywanie="0"
  l1="1"
  l2="2"
  l3="3"
  l4="4"
  l5="5"
  l6="6"
  l7="7"
  l8="8"
  l9="9"
  liczba="0"
  niepis="0"
  echo "1) Gra 1 vs komputer"
  echo "2) Gra 1 vs 1"
  echo "3) Wczytaj zapisane rozgrywki"
  echo ""
  echo "Wpisz liczbę odpowiadającą trybowi w który chcesz zagrać:"
  read trybb
  echo ""                                  #wartości startowe \/
  if [ $trybb = "3" ] ; then
  if [ -e zapis_slot1.sh ] ; then
  slot1="zajęty"
  else 
  slot1="wolny "
  fi
  if [ -e zapis_slot2.sh ] ; then
  slot2="zajęty"
  else 
  slot2="wolny "
  fi
  if [ -e zapis_slot3.sh ] ; then
  slot3="zajęty"
  else 
  slot3="wolny "
  fi
  if [ -e zapis_slot4.sh ] ; then
  slot4="zajęty"
  else 
  slot4="wolny "
  fi
  if [ -e zapis_slot5.sh ] ; then
  slot5="zajęty"
  else 
  slot5="wolny "
  fi
  if [ -e zapis_slot6.sh ] ; then
  slot6="zajęty"
  else 
  slot6="wolny "
  fi
  echo "   |   1    |   2    |   3    |" 
  echo "   | $slot1 | $slot2 | $slot3 |"
  echo "    --------------------------"
  echo "   | $slot4 | $slot5 | $slot6 |"
  echo "   |   4    |   5    |   6    |"  
  echo ""
  echo " Proszę wybrać slot do załadowania:"
  read slotdoz
  if [ $slotdoz = "1" ] ; then
  wwybranyslot="slot1"
  elif [ $slotdoz = "2" ] ; then
  wwybranyslot="slot2"
  elif [ $slotdoz = "3" ] ; then
  wwybranyslot="slot3"
  elif [ $slotdoz = "4" ] ; then
  wwybranyslot="slot4"
  elif [ $slotdoz = "5" ] ; then
  wwybranyslot="slot5"
  elif [ $slotdoz = "6" ] ; then
  wwybranyslot="slot6"
  fi
  if [ -e zapis_$wwybranyslot.sh ] ; then
  chmod 777 zapis_$wwybranyslot.sh
  ./zapis_$wwybranyslot.sh
  export wczytywanie="1"
  else
  echo " Przykro mi slot numer $slotdoz jest pusty, i nie można z niego załadować zapisu."
  break
  fi
  fi
  echo " $trybb - tryb"
  echo "$gracz1 - gracz"
  if [ $trybb = "1" ] ; then
  sleep 1
  echo ""
  if [ $wczytywanie = "0" ] ; then 
  echo "Proszę wybrać poziom trudności:"
  echo ""
  echo "1)Łatwy"
  echo "2)Normalny"
  echo "3)Trudny"
  echo ""
  echo "Proszę wpisac  liczbę odpowiadający wybranemu poziomowi trudności:"
  read poztr
  else
  echo ""
  fi
  if [ $poztr = "1" ] ; then
  echo ""
  elif [ $poztr = "2" ] ; then
  echo ""
  elif [ $poztr = "3" ] ; then
  echo ""
  else
  echo "prosze podać prawidłowy numer trybu"
  break
  fi
  echo ""
  if [ $wczytywanie = "0" ] ; then 
  echo "Proszę podać nick gracza:"
  read gracz1
  echo ""
  if [ -z $gracz1 ] ; then
   echo ""
   echo "Nazwa gracza nie może być pusta :("
   echo ""
   exit
  else
  sleep 1
  fi
  echo "Przeciwnikiem będzie komputer"
  echo ""
  echo "Rozpoczynanie rozgrywki."
  sleep 1
  echo ""
  echo "Rozpoczynanie rozgrywki..."
  sleep 1
  echo ""
  echo "Trwa losowanie zaczynającego..."
  sleep 1
  echo ""
  else
  echo ""
  fi
  let losowe=$RANDOM%2+1
  if [ $wczytywanie = "0" ] ; then 
  if [ $losowe = "1" ] ; then
  echo "Rozpoczyna gracz"
  gz="1"
  else
  echo "Rozpoczyna komputer"
  gz="2"
  fi   
  else
  echo ""
  fi    #PONIŻEJ WŁAŚCIWA GRA   #PONIŻEJ WŁAŚCIWA GRA     #PONIŻEJ WŁAŚCIWA GRA      #PONIŻEJ WŁAŚCIWA GRA      #PONIŻEJ WŁAŚCIWA GRA
  echo ""
  echo "Gracz - X"
  echo ""
  echo "Komputer - O"
  echo ""
  echo ""
  echo "Rozpoczynanie..."
  while [ $liczba -le "11" ] ; do
  if [ $l1!="1" ] &&  [ $l2!="2" ] && [ $l3!="3" ] && [ $l4!="4" ] && [ $l5!="5" ] && [ $l6!="6" ] && [ $l7!="7" ] && [ $l8!="8" ] && [ $l9!="9" ] ; then
  gameover="1"
  fi
                      #losowanie taktyk:
  taktyka=$RANDOM%4+1
  liczba=$[liczba + 1]
  #echo "Numer ruchu: $liczba"
  if [ $liczba -lt "10" ] ; then
  if [ $gz = "1" ] || [ $niepis = "0" ] ; then
  echo ""
  echo " $l1 | $l2 | $l3 "
  echo "-----------"
  echo " $l4 | $l5 | $l6 "
  echo "-----------"
  echo " $l7 | $l8 | $l9 "
  else 
  nicnierobie="1"
  fi
  if [ $l1 = "X" ] && [ $l2 = "X" ] && [ $l3 = "X" ] ; then
  liczba=$[$liczba-"1"] 
  lrdz=$liczba #lrdz liczba ruchów do zwycięstwa
  zw=$gracz1
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  gz="0"
  liczba="10"
  elif [ $l4 = "X" ] && [ $l5 = "X" ] && [ $l6 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l7 = "X" ] && [ $l8 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "X" ] && [ $l4 = "X" ] && [ $l7 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l2 = "X" ] && [ $l5 = "X" ] && [ $l8 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "X" ] && [ $l6 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "X" ] && [ $l5 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "X" ] && [ $l5 = "X" ] && [ $l7 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l7 = "O" ] && [ $l8 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l4 = "O" ] && [ $l7 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l2 = "O" ] && [ $l5 = "O" ] && [ $l8 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "O" ] && [ $l6 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l5 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "O" ] && [ $l5 = "O" ] && [ $l7 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw="komputer"
  gz="0"
  echo "Komputer wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 != "1" ] &&  [ $l2 != "2" ] && [ $l3 != "3" ] && [ $l4 != "4" ] && [ $l5 != "5" ] && [ $l6 != "6" ] && [ $l7! = "7" ] && [ $l8 != "8" ] && [ $l9 != "9" ] ; then
  break
  else
  nicnierobie="1"
  fi
  #echo "Zwycięstwo w $lrdz ruchach."
  if [ $gz = "1" ] ; then
  echo ""
  echo "Ruch gracza $gracz1:" 
  echo "(Jeżeli chcesz zapisać rozgrywkę w tym momencie wpisz \"zapisz\")"
  echo ""
  read ruch
  elif [ $gz = "2" ] && [ $poztr = "1" ] && [ $niepis = "1" ]  ; then         #PONIŻEJ RUCH KOMPUTERA 1 poz
  let ruch=$RANDOM%9+1
  elif [ $gz = "2" ] && [ $poztr = "1" ] && [ $niepis = "0" ]  ; then 
  echo ""
  echo "Ruch komputera.."
  echo ""
  echo "..."
  let ruch=$RANDOM%9+1
  elif [ $gz = "2" ] && [ $poztr = "2" ] && [ $niepis = "1" ]  ; then         #PONIŻEJ RUCH KOMPUTERA 2 poz
  if [ $l2 = "O" ] && [ $l3 = "O" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "O" ] && [ $l3 = "O" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "O" ] && [ $l6 = "O" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "O" ] && [ $l6 = "O" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "O" ] && [ $l9 = "O" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "O" ] && [ $l9 = "O" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "O" ] && [ $l7 = "O" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  else
  let ruch=$RANDOM%9+1 
  fi 
  elif [ $gz = "2" ] && [ $poztr = "2" ] && [ $niepis = "0" ]  ; then                  
  echo ""
  echo "Ruch komputera.."
  echo ""
  echo "..."
  if [ $l2 = "O" ] && [ $l3 = "O" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "O" ] && [ $l3 = "O" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "O" ] && [ $l6 = "O" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "O" ] && [ $l6 = "O" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "O" ] && [ $l9 = "O" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "O" ] && [ $l9 = "O" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "O" ] && [ $l7 = "O" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  else
  let ruch=$RANDOM%9+1 c
  fi           
                                                                              #PONIŻEJ RUCH KOMPUTERA 3 poz
#if [ $gz = "2" ] && [ $poztr = "3" ] && [ $taktyka = "1" ] ; then
  elif [ $gz = "2" ] && [ $poztr = "3" ] && [ $niepis = "0" ]  ; then                  
  echo ""
  echo "Ruch komputera.."
  echo ""
  echo "..."
  if [ $l5 = "5" ] ; then
  ruch="5"
  elif [ $l2 = "X" ] && [ $l3 = "X" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "X" ] && [ $l3 = "X" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "X" ] && [ $l2 = "X" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "X" ] && [ $l6 = "X" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "X" ] && [ $l6 = "X" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "X" ] && [ $l5 = "X" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "X" ] && [ $l9 = "X" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "X" ] && [ $l9 = "X" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "X" ] && [ $l7 = "X" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  elif [ $l2 = "O" ] && [ $l3 = "O" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "O" ] && [ $l3 = "O" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "O" ] && [ $l6 = "O" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "O" ] && [ $l6 = "O" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "O" ] && [ $l9 = "O" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "O" ] && [ $l9 = "O" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "O" ] && [ $l7 = "O" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  else
  let ruch=$RANDOM%9+1 
  fi          
  elif [ $gz = "2" ] && [ $poztr = "3" ] && [ $niepis = "1" ]  ; then   
  if [ $l5 = "5" ] ; then
  ruch="5"      
  elif [ $l2 = "X" ] && [ $l3 = "X" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "X" ] && [ $l3 = "X" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "X" ] && [ $l2 = "X" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "X" ] && [ $l6 = "X" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "X" ] && [ $l6 = "X" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "X" ] && [ $l5 = "X" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "X" ] && [ $l9 = "X" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "X" ] && [ $l9 = "X" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "X" ] && [ $l7 = "X" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  elif [ $l2 = "O" ] && [ $l3 = "O" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  ruch="1"
  elif [ $l1 = "O" ] && [ $l3 = "O" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  ruch="2"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  ruch="3" 
  elif [ $l5 = "O" ] && [ $l6 = "O" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  ruch="4" 
  elif [ $l4 = "O" ] && [ $l6 = "O" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  ruch="5"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  ruch="6"  
  elif [ $l8 = "O" ] && [ $l9 = "O" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  ruch="7" 
  elif [ $l7 = "O" ] && [ $l9 = "O" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  ruch="8" 
  elif [ $l8 = "O" ] && [ $l7 = "O" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  ruch="9"
  else
  let ruch=$RANDOM%9+1 
  fi              
#elif [ $gz = "2" ] && [ $poztr = "3" ] && [ $taktyka = "2" ] ; then
  #elif [ $gz = "2" ] && [ $poztr = "3" ] && [ $niepis = "0" ]  ; then                  
  #echo ""
  #echo "Ruch komputera.."
  #echo ""
  #echo "..."
  #if [ $l5 = "5" ] ; then
  #ruch="5"     



                          
  elif [ $gz = "0" ] ; then         #KONIEC RUCHU KOMPUTERA
  nicnierobie="1"
  else 
  echo ""
  echo "???"
  sleep 1
  exit
  fi
  if [ $ruch = "1" ] && [ $gz = "1" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  l1="X"
  gz="2"
  elif [ $ruch = "1" ] && [ $gz = "2" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  l1="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "2" ] && [ $gz = "1" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  l2="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "2" ] && [ $gz = "2" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  l2="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "3" ] && [ $gz = "1" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  l3="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "3" ] && [ $gz = "2" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  l3="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "4" ] && [ $gz = "1" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  l4="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "4" ] && [ $gz = "2" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  l4="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "5" ] && [ $gz = "1" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  l5="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "5" ] && [ $gz = "2" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  l5="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "6" ] && [ $gz = "1" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  l6="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "6" ] && [ $gz = "2" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  l6="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "7" ] && [ $gz = "1" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  l7="X" 
  gz="2"
  niepis="0"
  elif [ $ruch = "7" ] && [ $gz = "2" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  l7="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "8" ] && [ $gz = "1" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  l8="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "8" ] && [ $gz = "2" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  l8="O"
  gz="1"
  niepis="0"
  elif [ $ruch = "9" ] && [ $gz = "1" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  l9="X"
  gz="2"
  niepis="0"
  elif [ $ruch = "9" ] && [ $gz = "2" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  l9="O"
  gz="1"
  niepis="0"
  elif [ $gz = "0" ] ; then
  nicnierobie="1"
  echo ""
  elif [ $ruch = "zapisz" ] ; then
  echo "Czy chcesz przejść do zapisywania rozgrywki?"
  echo "T/N"
  echo ""
  read napewno
  if [ $napewno = "N" ] ; then
  echo "rozgrywka będzie kontynuowana bez zapisu"
  elif [ $napewno = "T" ] ; then
  echo ""
  echo "Dostępne sloty zapisu:"
  echo ""
  if [ -e zapis_slot1.sh ] ; then
  slot1="zajęty"
  else 
  slot1="wolny "
  fi
  if [ -e zapis_slot2.sh ] ; then
  slot2="zajęty"
  else 
  slot2="wolny "
  fi
  if [ -e zapis_slot3.sh ] ; then
  slot3="zajęty"
  else 
  slot3="wolny "
  fi
  if [ -e zapis_slot4.sh ] ; then
  slot4="zajęty"
  else 
  slot4="wolny "
  fi
  if [ -e zapis_slot5.sh ] ; then
  slot5="zajęty"
  else 
  slot5="wolny "
  fi
  if [ -e zapis_slot6.sh ] ; then
  slot6="zajęty"
  else 
  slot6="wolny "
  fi
  echo "|   1    |   2    |   3    |" 
  echo "| $slot1 | $slot2 | $slot3 |"
  echo " --------------------------"
  echo "| $slot4 | $slot5 | $slot6 |"
  echo "|   4    |   5    |   6    |"  
  echo ""
  echo "Proszę wybrać slot do zapisu:"
  read slotdozapisu
  if [ $slotdozapisu = "1" ] ; then
  wybranyslot="slot1"
  elif [ $slotdozapisu = "2" ] ; then
  wybranyslot="slot2"
  elif [ $slotdozapisu = "3" ] ; then
  wybranyslot="slot3"
  elif [ $slotdozapisu = "4" ] ; then
  wybranyslot="slot4"
  elif [ $slotdozapisu = "5" ] ; then
  wybranyslot="slot5"
  elif [ $slotdozapisu = "6" ] ; then
  wybranyslot="slot6"
  else
  echo "proszę wpisać właściwy numer slotu"
  fi
  echo ""
  if [ -e zapis_$wybranyslot.sh ] ; then
  echo "Wybrany slot jest zajęty, czy chcesz go nadpisać?"
  echo "T/N"
  read nadpisanie
  echo ""
   if [ $nadpisanie = "N" ] ; then
    echo "slot nie zostanie nadpisany"
   elif [ $nadpisanie = "T" ] ; then
    echo "zapisywanie..."
    echo ""
    zapisywanie_rozgrywki
    echo ""
    echo " Czy chcesz wyjść do menu?"
    echo "T/N"
    read wyjscdom
    echo ""
   if [ $wyjscdom = "T" ] ; then
    break
   fi
   fi
  else
   echo "zapisywanie..."
   echo ""
   zapisywanie_rozgrywki
   echo ""
   echo " Czy chcesz wyjść do menu?"
   echo "T/N"
   read wyjscdom
   echo ""
    if [ $wyjscdom = "T" ] ; then
    break
    fi
   fi
  else 
   slot6="wolny "
  fi
  elif [ $gz = "1" ] ; then
  echo ""
  echo "??"
  echo ""
  echo "Proszę sprawdzić pole w które się wpisało liczbę!"
  echo ""
  niepis="1" 
  let liczba=$liczba-'1'
  else
  niepis="1" 
  let liczba=$liczba-'1'
  nicnierobie="1"
  fi
  if [ $gz = "0" ] ; then
  echo ""
  else
  nicnierobie="1"
  fi
fi
done
if [ $gz = "1" ] && [ $zapisano = "0" ] || [ $gz = "2" ] && [ $zapisano = "0" ] ; then
zw="REMIS"
lrdz="-"
echo "Jest remis!"
else
echo ""
fi
  elif [ $trybb = "2" ] ; then    #PONIŻEJ TRYB 1 VS 2    #PONIŻEJ TRYB 1 VS 2      #PONIŻEJ TRYB 1 VS 2       #PONIŻEJ TRYB 1 VS 2       #PONIŻEJ TRYB 1 VS 2       
  echo ""
  echo "Rozpoczynanie nowej gry..."
  echo ""
  echo "Proszę podać nick 1 - szego gracza:"
  read gracz1
  echo ""
  if [ -z $gracz1 ] ; then
   echo ""
   echo "Nazwa gracza nie może być pusta :("
   echo ""
   exit
  else
  echo ""
  fi
  echo "Proszę podać nick 2 - giego gracza:"
  read gracz2
  echo ""
  if [ -z $gracz2 ] ; then
   echo ""
   echo "Nazwa gracza nie może być pusta :("
   echo ""
   exit
  else
  echo ""
  fi
  if [ $gracz1 = $gracz2 ] ; then
  echo ""
  echo "Nazwy graczy nie powinny byc takie same :/"
  echo ""
  exit
  else
  echo ""
  fi
  echo ""
  echo "Gracz 1 - X"
  echo ""
  echo "Gracz 2 - O"
  echo ""

  let gz=$RANDOM%2+1
  if [ $gz = '1' ] ; then
  echo "Gracz $gracz1 będzie zaczynał rozgrywkę:"
  gz="1"
  else
  echo "Gracz $gracz2 będzie zaczynał rozgrywkę:"
  gz="2"
  fi 
  
  liczba="0"
  echo ""
  echo "Rozpoczynanie..."
  l1="1"
  l2="2"
  l3="3"
  l4="4"
  l5="5"
  l6="6"
  l7="7"
  l8="8"
  l9="9"
  while [ $liczba -le "9" ] ; do
  liczba=$[liczba + 1]
  echo ""
  echo " $l1 | $l2 | $l3 "
  echo "-----------"
  echo " $l4 | $l5 | $l6 "
  echo "-----------"
  echo " $l7 | $l8 | $l9 "
  if [ $l1 = "X" ] && [ $l2 = "X" ] && [ $l3 = "X" ] ; then
  lrdz=$liczba #lrdz liczba ruchów do zwycięstwa
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  gz="0"
  liczba="10"
  elif [ $l4 = "X" ] && [ $l5 = "X" ] && [ $l6 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l7 = "X" ] && [ $l8 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "X" ] && [ $l4 = "X" ] && [ $l7 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l2 = "X" ] && [ $l5 = "X" ] && [ $l8 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "X" ] && [ $l6 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "X" ] && [ $l5 = "X" ] && [ $l9 = "X" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "X" ] && [ $l5 = "X" ] && [ $l7 = "X" ] ; then 
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz1
  gz="0"
  echo "Brawo gracz $gracz1 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l2 = "O" ] && [ $l3 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l4 = "O" ] && [ $l5 = "O" ] && [ $l6 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l7 = "O" ] && [ $l8 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l4 = "O" ] && [ $l7 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l2 = "O" ] && [ $l5 = "O" ] && [ $l8 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "O" ] && [ $l6 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l1 = "O" ] && [ $l5 = "O" ] && [ $l9 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  elif [ $l3 = "O" ] && [ $l5 = "O" ] && [ $l7 = "O" ] ; then
  lrdz=$liczba 
  lrdz=$[$lrdz-"1"]
  zw=$gracz2
  gz="0"
  echo "Brawo gracz $gracz2 wygral w $lrdz ruchach!"
  liczba="10"
  else
  echo ""
  fi
  if [ $liczba -le "9" ] ; then
  if [ $gz = "1" ] ; then
  echo ""
  echo "Ruch gracza $gracz1:" 
  read ruch
  elif [ $gz = "2" ] ; then
  echo ""
  echo "Ruch gracza $gracz2:"
  read ruch
  elif [ $gz = "0" ] ; then
  echo ""
  else 
  echo ""
  echo "???"
  sleep 1
  exit
  fi
  if [ $ruch = "1" ] && [ $gz = "1" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  l1="X"
  gz="2"
  elif [ $ruch = "1" ] && [ $gz = "2" ] && [ $l1 != "O" ] && [ $l1 != "X" ] ; then
  l1="O"
  gz="1"
  elif [ $ruch = "2" ] && [ $gz = "1" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  l2="X"
  gz="2"
  elif [ $ruch = "2" ] && [ $gz = "2" ] && [ $l2 != "O" ] && [ $l2 != "X" ] ; then
  l2="O"
  gz="1"
  elif [ $ruch = "3" ] && [ $gz = "1" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  l3="X"
  gz="2"
  elif [ $ruch = "3" ] && [ $gz = "2" ] && [ $l3 != "O" ] && [ $l3 != "X" ] ; then
  l3="O"
  gz="1"
  elif [ $ruch = "4" ] && [ $gz = "1" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  l4="X"
  gz="2"
  elif [ $ruch = "4" ] && [ $gz = "2" ] && [ $l4 != "O" ] && [ $l4 != "X" ] ; then
  l4="O"
  gz="1"
  elif [ $ruch = "5" ] && [ $gz = "1" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  l5="X"
  gz="2"
  elif [ $ruch = "5" ] && [ $gz = "2" ] && [ $l5 != "O" ] && [ $l5 != "X" ] ; then
  l5="O"
  gz="1"
  elif [ $ruch = "6" ] && [ $gz = "1" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  l6="X"
  gz="2"
  elif [ $ruch = "6" ] && [ $gz = "2" ] && [ $l6 != "O" ] && [ $l6 != "X" ] ; then
  l6="O"
  gz="1"
  elif [ $ruch = "7" ] && [ $gz = "1" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  l7="X" 
  gz="2"
  elif [ $ruch = "7" ] && [ $gz = "2" ] && [ $l7 != "O" ] && [ $l7 != "X" ] ; then
  l7="O"
  gz="1"
  elif [ $ruch = "8" ] && [ $gz = "1" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  l8="X"
  gz="2"
  elif [ $ruch = "8" ] && [ $gz = "2" ] && [ $l8 != "O" ] && [ $l8 != "X" ] ; then
  l8="O"
  gz="1"
  elif [ $ruch = "9" ] && [ $gz = "1" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  l9="X"
  gz="2"
  elif [ $ruch = "9" ] && [ $gz = "2" ] && [ $l9 != "O" ] && [ $l9 != "X" ] ; then
  l9="O"
  gz="1"
  elif [ $gz = "0" ] ; then
  echo ""
  else 
  echo ""
  echo "??"
  echo ""
  echo "Proszę sprawdzić pole w które się wpisało liczbę!"
  echo ""
  let liczba=$liczba-'1'
  fi
  fi
  if [ $gz = "0" ] ; then
  echo ""
  else
  echo ""
  fi
done
if [ $gz = "1" ] && [ $zapisano = "0" ] || [ $gz = "2" ] && [ $zapisano = "0" ] ; then
zw="REMIS"
lrdz="-"
echo "Jest remis!"
else
echo ""
fi
else 
echo "Proszę wybrać tryb"
break
fi
 ;;

  "Zobacz/Wczytaj/Usuń zapisane rozgrywki")
  clear
  echo ""
  if [ -e zapis_slot1.sh ] ; then
  slot1="zajęty"
  else 
  slot1="wolny "
  fi
  if [ -e zapis_slot2.sh ] ; then
  slot2="zajęty"
  else 
  slot2="wolny "
  fi
  if [ -e zapis_slot3.sh ] ; then
  slot3="zajęty"
  else 
  slot3="wolny "
  fi
  if [ -e zapis_slot4.sh ] ; then
  slot4="zajęty"
  else 
  slot4="wolny "
  fi
  if [ -e zapis_slot5.sh ] ; then
  slot5="zajęty"
  else 
  slot5="wolny "
  fi
  if [ -e zapis_slot6.sh ] ; then
  slot6="zajęty"
  else 
  slot6="wolny "
  fi
  echo "   |   1    |   2    |   3    |" 
  echo "   | $slot1 | $slot2 | $slot3 |"
  echo "    --------------------------"
  echo "   | $slot4 | $slot5 | $slot6 |"
  echo "   |   4    |   5    |   6    |"  
  echo ""
  echo "Proszę wpisać numer wybranego slotu:"
  read numersslotu
  echo ""
  if [ $numersslotu = "1" ] ; then
   wybrany_slot1="zapis_slot1.sh"
  elif [ $numersslotu = "2" ] ; then
   wybrany_slot1="zapis_slot2.sh"
  elif [ $numersslotu = "3" ] ; then
   wybrany_slot1="zapis_slot3.sh"
  elif [ $numersslotu = "4" ] ; then
   wybrany_slot1="zapis_slot4.sh"
   elif [ $numersslotu = "5" ] ; then
   wybrany_slot1="zapis_slot5.sh"
  elif [ $numersslotu = "6" ] ; then
   wybrany_slot1="zapis_slot6.sh"
   else
   echo " Podany numer slotu jest nieprawidłowy"
   fi
   echo ""
  select y in "Zobacz" "Wczytaj" "Usuń" "Wyjście do menu"
   do
    case $y in 
	"Zobacz")
	 if [ -e $wybrany_slot1 ] ; then
	 echo ""
	 chmod  777 "$wybrany_slot1"
	 ./"$wybrany_slot1"
	 else
	 echo "Podany zapis nie istnieje, lub został usunięty"
	 fi
	;;
	"Wczytaj")
	
	;;
	"Usuń")
	
	;;
	"Wyjście do menu")
	break
	esac
done
;;
  "Zapisy")
  echo ""
  echo "Witaj w systemie zapisów!"
  echo ""
  echo "Co chcesz zrobić ze swoim wynikiem?"
  echo ""
  select y in "Dodaj do zapisów" "Zobacz zapisane" "Usuń zapisy" "Wyjście do menu głównego"
  do
  case $y in 
  "Dodaj do zapisów")
  echo ""
  echo "Zapisane zostaną nicki graczy, wynik/remis, liczba ruchów do zwycięstwa/remisu oraz data."
  echo ""
  echo "Kontynuować? (T/N)"
  read mozetak
  if [ $mozetak = "T" ] ; then
  echo ""
  echo "Rozpoczynanie procedury zapisu rozgrywki..."
  sleep 1
  echo ""
  if [ -e $PWD/'historia rozgrywek.txt' ] ; then
  echo "Wynik zostanie dopisany do pozostałych..."
  sleep 1
  else
  echo "Jest to pierwszy zapis więc zostanie utworzony plik z wynikami..."
  sleep 1
  echo ""
  echo "Tworzenie pliku z wynikami..."
  sleep 1
  echo ""
  echo "Plik z wynikami został utworzony..."
  fi
  echo ""
  echo "Zapisywanie trybu..."
  if [ $trybb = "2" ] ; then
  echo "Tryb: 1 vs 1" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  echo "Zapisywanie nazw graczy..."
  echo "Gracze: $gracz1 vs $gracz2" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  elif [ $trybb = "1" ] ; then
  echo "Tryb: 1 vs komputer" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  echo "Zapisywanie poziomu trudności..."
  if [ $poztr = "1" ] ; then
  echo "Poziom trudności: Łatwy" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  elif [ $poztr = "2" ] ; then
  echo "Poziom trudności: Normalny" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  elif [ $poztr = "3" ] ; then
  echo "Poziom trudności: Trudny" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  else
  echo "Poziom trudności: ???" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
fi
  echo "Zapisywanie nazwy gracza..."
  echo "Gracz: $gracz1 " >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
fi
  echo "Zapisywanie wyniku..."
  echo "Zwycięzca: $zw" >> $PWD/'historia rozgrywek.txt' 
  sleep 1
  echo ""
  echo "Zapisywanie liczby wykonanych ruchów..."
  echo "Liczba wykonanych ruchów: $lrdz" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  echo "Czy chcesz podac datę?"
  echo ""
  echo "(T/N)"
  read mozenie
  echo ""
  if [ $mozenie = "T" ] ; then
  echo "Prosze podać dzisiejszą datę:"
  read ddata
  echo ""
  echo "Zapisywanie podanej daty..."
  echo "Data: $ddata" >> $PWD/'historia rozgrywek.txt'
  sleep 1
  echo ""
  sleep 1
  echo "Finalizacja zapisu..."
  sleep 1
  echo "-----------" >> $PWD/'historia rozgrywek.txt'
  echo ""
  echo "Zapisywanie zakończone."
  else 
  sleep 1
  echo "Finalizacja zapisu..."
  sleep 1
  echo "-----------" >> $PWD/'historia rozgrywek.txt'
  echo ""
  echo "Zapisywanie zakończone."
  fi
  else
  echo ""
  echo "Anulowanie zapisu..."
  sleep 1
  echo ""
  echo "Nie zapisano."
  fi
  ;;
  "Zobacz zapisane")
  echo ""
  if [ -e $PWD/'historia rozgrywek.txt' ] ; then
  echo ''
  more 'historia rozgrywek.txt'
  echo ""
  else
  echo ""
  echo "Nie ma co wyswietlić :/"
  echo "Stworzyć go można poprzez zapisywanie rozgrywek"
  fi
  ;;
  "Usuń zapisy")
  if [ -e $PWD/'historia rozgrywek.txt' ] ; then
  echo ""
  echo 'Zapis rozgrywek zostanie usunięty'
  echo 'Kontynuować? (T/N)'
  read usun
  case "$usun" in
  "T") echo "" 
       echo 'Usuwanie...' 
       rm 'historia rozgrywek.txt' 
       echo "" 
       sleep 1 
       echo 'Gotowe'
       ;;
  "*") echo ""  
       echo 'Historia rozgrywek nie zostanie usunięta...'
       echo ""
  esac
  else
  echo ""
  echo "Nie ma co wyswietlić :/"
  echo "Stworzyć go można poprzez zapisywanie rozgrywek"
  fi
  ;;
  "Wyjście do menu głównego")
  echo ""
  break
  esac
  done
  ;;
  "Jak grać?")
  echo ""
  l1=""
  l2=""
  l3=""
  echo "W grze kółko i krzyżyk chodzi o to aby ułożyć X lub O w jednej lini w polu:"
  echo " $l1  |  $l2 |  $l3 "
  echo "-----------"
  echo " $l4  | $l5  |  $l6 "
  echo "-----------"
  echo " $l7  |  $l8 |  $l9 "
  echo ""
  sleep 1
  echo "Np."
  echo ""
  l1=X
  l2=X
  l3=X
  echo " $l1 | $l2 |  $l3 "
  echo "-----------"
  echo " $l4  | $l5  |  $l6 "
  echo "-----------"
  echo " $l7  | $l8  |  $l9 "
  echo ""
  sleep 1
  l1=X
  l4=X
  l7=X
  echo "lub"
  l2=""
  l3=""
  echo ""
  echo " $l1  |  $l2 |  $l3 "
  echo "-----------"
  echo " $l4  | $l5  |  $l6 "
  echo "-----------"
  echo " $l7  |  $l8 |  $l9 "
  echo ""
  echo "Czy chcesz odwiedzić stronę internetową gdzie będzie opisane więcej o grze i jej zasadach?"
  echo "Kontynuj (T/N)"
  read kontynuj
  echo ""
  if [ $kontynuj = "T" ] ; then
  echo "Trwa przekierowywanie."
  sleep 1
  echo ""
  echo "Trwa przekierowywanie.."
  sleep 1
  echo ""
  echo "Trwa przekierowywanie..."
  sleep 1
  echo ""
  echo "https://pl.wikipedia.org/wiki/K%C3%B3%C5%82ko_i_krzy%C5%BCyk"
  #source <(curl -s https://pl.wikipedia.org/wiki/K%C3%B3%C5%82ko_i_krzy%C5%BCyk)>
  echo ""
  echo "(klikinij link)"
  echo ""
  else
  echo ""
  fi
  ;;
   "Autor")
   echo ""
   echo "Autorem niniejszej gry jest Łukasz Kocielnik," 
   echo "\"Pan Robaczek\""
   echo ""
   echo "Gra powstała w," 
   echo "Luty/Marzec 2020"
   echo ""
   echo "Miłej gry!"
  ;;
  "Wyjście")
   echo ""
   sleep 1
   echo "Wyłączanie..."
   echo ""
   echo "( . )"
   echo ""
   sleep "1"
   echo "( .. )"
   echo ""
   sleep 1
   echo "( ... )"
   sleep 1
   echo ""
   echo "Do zobaczenia!"
   echo ""
   echo ""
   exit
 esac
done
